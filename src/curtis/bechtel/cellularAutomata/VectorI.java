package curtis.bechtel.cellularAutomata;

public class VectorI {
	
	public int x;
	public int y;
	public int z;
	
	public VectorI(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public VectorI add(VectorI vec) {
		return new VectorI(x + vec.x, y + vec.y, z + vec.z);
	}
	
	public VectorI add(int dx, int dy, int dz) {
		return new VectorI(x + dx, y + dy, z + dz);
	}
	
	public VectorI difference(VectorI vec) {
		return new VectorI(x - vec.x, y - vec.y, z - vec.z);
	}
	
	public int getTaxicabMagnitude() {
		return Math.abs(x) + Math.abs(y) + Math.abs(z);
	}
	
	public int getMaximumMagnitude() {
		return Math.max(Math.max(Math.abs(x), Math.abs(y)), Math.abs(z));
	}
	
	
	
	
	
	public VectorD toVectorD() {
		return new VectorD(x, y, z);
	}
	
	
	
	
	
	@Override
	public int hashCode() {
		return x ^ (y * 17) ^ (z * 31);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj.getClass() != VectorI.class) {
			return false;
			
		} else {
			VectorI vec = (VectorI) obj;
			
			return vec.x == x && vec.y == y && vec.z == z;
		}
	}
	
}
