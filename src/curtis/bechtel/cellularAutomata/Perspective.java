package curtis.bechtel.cellularAutomata;

public class Perspective {
	
	public static final double TAU = 2 * Math.PI;
	public static final double HALF_PI = .5 * Math.PI;
	
	public static final double DEFAULT_HORIZONTAL_ANGLE = 0;
	public static final double DEFAULT_VERTICAL_ANGLE = 0;
	
	
	
	
	
	private double horizontalAngle;
	private double verticalAngle;
	
	private double sinVerticalAngle;
	private double cosVerticalAngle;
	private double sinHorizontalAngle;
	private double cosHorizontalAngle;
	
	private VectorD position;
	
	
	
	
	
	public Perspective(VectorD position) {
		this.position = position;
		
		this.setHorizontalAngle(DEFAULT_HORIZONTAL_ANGLE);
		this.setVerticalAngle(DEFAULT_VERTICAL_ANGLE);
	}
	
	
	
	
	
	public VectorD getAdjustedPosition(VectorD vec) {
		VectorD temp = vec.difference(position);
		
		double dy2 = temp.x * cosHorizontalAngle - temp.y * sinHorizontalAngle;
		
		double dx = temp.z * sinVerticalAngle + dy2 * cosVerticalAngle;
		double dy = temp.x * sinHorizontalAngle + temp.y * cosHorizontalAngle;
		double dz = temp.z * cosVerticalAngle - dy2 * sinVerticalAngle;
		
		temp.x = dx;
		temp.y = dy;
		temp.z = dz;
		
		return temp;
	}
	
	
	
	
	
	public void move(VectorD p) {
		setPosition(position.add(p));
	}
	
	public void moveForward(double distance) {
		move(new VectorD(cosHorizontalAngle * distance, -sinHorizontalAngle * distance, 0));
	}
	
	public void moveSideways(double distance) {
		move(new VectorD(sinHorizontalAngle * distance, cosHorizontalAngle * distance, 0));
	}
	
	public void turnHorizontal(double angle) {
		setHorizontalAngle(horizontalAngle + angle);
	}
	
	public void turnVertical(double angle) {
		setVerticalAngle(verticalAngle + angle);
	}
	
	
	
	
	
	public void setPosition(VectorD position) {
		this.position = position;
	}
	
	public void setHorizontalAngle(double angle) {
		this.horizontalAngle = (angle % TAU + TAU) % TAU;
		
		this.sinHorizontalAngle = Math.sin(horizontalAngle);
		this.cosHorizontalAngle = Math.cos(horizontalAngle);
	}
	
	public void setVerticalAngle(double angle) {
		this.verticalAngle = (angle > HALF_PI) ? HALF_PI : (angle < -HALF_PI) ? -HALF_PI : angle;
		
		this.sinVerticalAngle = Math.sin(verticalAngle);
		this.cosVerticalAngle = Math.cos(verticalAngle);
	}
	
	
	
	
	
	public VectorD getPosition() {
		return position;
	}
	
	public double getVerticalAngle() {
		return verticalAngle;
	}
	
	public double getHorizontalAngle() {
		return horizontalAngle;
	}
	
}
