package curtis.bechtel.cellularAutomata;

import java.util.ArrayList;

public class QueuedArray<Type> {
	
	private ArrayList<Type> array = new ArrayList<Type>();
	
	private ArrayList<Type> additionQueue = new ArrayList<Type>();
	private ArrayList<Type> deletionQueue = new ArrayList<Type>();
	
	public void add(Type obj) {
		additionQueue.add(obj);
	}
	
	public void remove(Type obj) {
		deletionQueue.add(obj);
	}
	
	public Type get(int index) {
		return array.get(index);
	}
	
	public ArrayList<Type> getArray() {
		return new ArrayList<Type>(array);
	}
	
	public boolean isListed(Type obj) {
		return array.contains(obj);
	}
	
	public boolean isEmpty() {
		return array.isEmpty();
	}
	
	public int size() {
		return array.size();
	}
	
	public void update() {
		while (additionQueue.size() > 0) {
			array.add(additionQueue.get(0));
			additionQueue.remove(0);
		}
		
		while (deletionQueue.size() > 0) {
			array.remove(deletionQueue.get(0));
			deletionQueue.remove(0);
		}
	}
	
}
