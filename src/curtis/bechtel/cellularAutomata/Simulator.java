package curtis.bechtel.cellularAutomata;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import curtis.bechtel.cellularAutomata.ruleSets.Lasers;
import curtis.bechtel.tools.EventHandler;
import curtis.bechtel.tools.LoopHandler;
import curtis.bechtel.tools.MainLoop;
import curtis.bechtel.tools.Window;

public class Simulator implements EventHandler, LoopHandler {
	
	public static final RuleSet DEFAULT_RULESET = new Lasers();
	
	public static final int POS_X = 0; // positive x direction
	public static final int POS_Y = 1; // positive y direction
	public static final int POS_Z = 2; // positive z direction
	public static final int NEG_X = 3; // negative x direction
	public static final int NEG_Y = 4; // negative y direction
	public static final int NEG_Z = 5; // negative z direction
	
	private static final double SCREEN_DISTANCE = 0.15; // default to 0.15
	private static final double PIXELS_PER_METER = 3780; // default to 3780 assuming the screen is about about 96 DPI
	private static final double MAX_VIEW_DISTANCE = 640; // default to 32
	
	private static final int SHADING = 16; // used in coloring cell faces
	private static final int[][] FACE_POINTS = new int[][] {
			{4, 5, 7, 6}, // positive x face
			{2, 3, 7, 6}, // positive y face
			{1, 3, 7, 5}, // positive z face
			{0, 1, 3, 2}, // negative x face
			{0, 1, 5, 4}, // negative y face
			{0, 2, 6, 4} // negative z face
	}; // these numbers are the vertices needed to display the corresponding face
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		new Simulator().run();
	}
	
	public Simulator() {
		window = new Window(1600, 1000);
		window.setEventHandler(this);
		window.setName("Cellular Automata");
		window.setBackgroundColor(Color.BLACK);
		
		loop = new MainLoop(this, 20, 60);
		perspective = new Perspective(new VectorD(0.5, 5.5, 5.5));
		
		initializeStartingCells();
		frame();
	}
	
	private void initializeStartingCells() {
		addCell(2, new VectorI(10, 0, 1));
		addCell(1, new VectorI(11, 0, 1));
		addCell(1, new VectorI(9, 0, 1));
		// addCell(1, new VectorI(10, 1, 1));
		addCell(1, new VectorI(10, -1, 1));
		addCell(1, new VectorI(10, 0, 2));
		addCell(1, new VectorI(10, 0, 0));
		
		
		addCell(2, new VectorI(10, 10, 0));
		addCell(1, new VectorI(11, 10, 0));
		addCell(1, new VectorI(9, 10, 0));
		addCell(1, new VectorI(10, 11, 0));
		addCell(1, new VectorI(10, 9, 0));
		// addCell(1, new VectorI(10, 10, 1));
		addCell(1, new VectorI(10, 10, -1));
		
		
		addCell(2, new VectorI(10, 20, 10));
		addCell(1, new VectorI(11, 20, 10));
		addCell(1, new VectorI(9, 20, 10));
		addCell(1, new VectorI(10, 21, 10));
		// addCell(1, new VectorI(10, 19, 10));
		addCell(1, new VectorI(10, 20, 11));
		addCell(1, new VectorI(10, 20, 9));
		
		
		addCell(2, new VectorI(10, 0, 10));
		addCell(1, new VectorI(11, 0, 10));
		addCell(1, new VectorI(9, 0, 10));
		// addCell(1, new VectorI(10, 1, 10));
		addCell(1, new VectorI(10, -1, 10));
		addCell(1, new VectorI(10, 0, 11));
		addCell(1, new VectorI(10, 0, 9));
		
		
		addCell(2, new VectorI(10, 1, 11));
		addCell(1, new VectorI(11, 1, 11));
		addCell(1, new VectorI(9, 1, 11));
		addCell(1, new VectorI(10, 2, 11));
		addCell(1, new VectorI(10, 0, 11));
		addCell(1, new VectorI(10, 1, 12));
		// addCell(1, new VectorI(10, 1, 11));
		
		
		allCells = allCellsQueue;
	}
	
	
	
	
	
	
	
	
	private Window window;
	private MainLoop loop;
	private Perspective perspective;
	private boolean change = true; // true if there is a perspective or scenery change to be redrawn
	private int placementDistance = 4;
	private int placementType = 1;
	
	private RuleSet ruleSet = DEFAULT_RULESET;
	private HashMap<VectorI, Integer> allCells = new HashMap<>(); // entire list of cells
	private HashMap<VectorI, Integer> allCellsQueue = new HashMap<>(); // cells which will be
	private HashMap<VectorI, Integer> activeCells = new HashMap<>(); // currently active cells
	private HashMap<VectorI, Integer> activeCellsQueue = new HashMap<>(); // cells set to be active
	
	
	
	
	
	
	
	
	private TreeMap<Integer, TreeMap<Integer, TreeMap<Integer, Integer>>> cells = new TreeMap<>();
	
	private void add(VectorI position, int type) {
		if (type == 0) {
			remove(position);
			return;
		}
		
		if (cells.containsKey(position.z)) {
			if (cells.get(position.z).containsKey(position.y)) {
				cells.get(position.z).get(position.y).put(position.x, type);
				
			} else {
				cells.get(position.z).put(position.y, new TreeMap<>());
				cells.get(position.z).get(position.y).put(position.x, type);
			}
			
		} else {
			cells.put(position.z, new TreeMap<>());
			cells.get(position.z).put(position.y, new TreeMap<>());
			cells.get(position.z).get(position.y).put(position.x, type);
		}
	}
	
	private void remove(VectorI position) {
		if (cells.containsKey(position.z)) {
			if (cells.get(position.z).containsKey(position.y)) {
				cells.get(position.z).get(position.y).remove(position.x);
			}
		}
	}
	
	private int get(VectorI position) {
		if (cells.containsKey(position.z)) {
			if (cells.get(position.z).containsKey(position.y)) {
				if (cells.get(position.z).get(position.y).containsKey(position.x)) {
					return cells.get(position.z).get(position.y).get(position.x);
					
				} else {
					return 0;
				}
				
			} else {
				return 0;
			}
			
		} else {
			return 0;
		}
	}
	
	@SuppressWarnings("unchecked")
	private void order() {
		// TODO: rewrite storage and ordering of cells with treemaps
		
		TreeMap<Integer, TreeMap<Integer, Integer>>[] zValues = (TreeMap<Integer, TreeMap<Integer, Integer>>[]) cells.values().toArray();
		for (int z = 0; z < zValues.length; z++) {
			
			TreeMap<Integer, Integer>[] yValues = (TreeMap<Integer, Integer>[]) zValues[z].values().toArray();
			for (int y = 0; y < yValues.length; y++) {
				
				Integer[] xValues = (Integer[]) yValues[y].values().toArray();
				for (int x = 0; x < xValues.length; x++) {
					
					
				}
			}
		}
		
	}
	
	
	
	
	
	
	private void run() {
		loop.start();
	}
	
	@Override
	public void update() {
		allCells = allCellsQueue;
		activeCells = activeCellsQueue;
		
		System.out.println("ACTIVE: " + activeCells.size());
		System.out.println("ALL: " + allCells.size());
		System.out.println();
		
		if (activeCells.size() > 0) {
			change = true;
		}
		
		ArrayList<VectorI> cellList = new ArrayList<>(activeCells.keySet());
		
		for (int i = 0; i < cellList.size(); i++) {
			ruleSet.updateCell(cellList.get(i), this);
		}
	}
	
	public void addCell(Integer type, VectorI position) {
		if (type == 0) {
			removeCell(position);
			return;
		}
		if (position.getMaximumMagnitude() > 32000) {
			return;
		}
		allCellsQueue.put(position, type);
		activateCell(position);
	}
	
	public void removeCell(VectorI position) {
		allCellsQueue.remove(position);
		deactivateCell(position);
	}
	
	public void activateCell(VectorI position) {
		activeCellsQueue.put(position, 0);
	}
	
	public void activateNeighbors(VectorI position) {
		activateCell(position);
		activateCell(position.add(1, 0, 0));
		activateCell(position.add(0, 1, 0));
		activateCell(position.add(0, 0, 1));
		activateCell(position.add(-1, 0, 0));
		activateCell(position.add(0, -1, 0));
		activateCell(position.add(0, 0, -1));
	}
	
	public void deactivateCell(VectorI position) {
		activeCellsQueue.remove(position);
	}
	
	public Integer getCell(VectorI position) {
		if (allCells.containsKey(position)) {
			return allCells.get(position);
			
		} else {
			return 0;
		}
	}
	
	
	
	
	
	
	
	
	private void controller() {
		if (window.isButtonPressed(Window.BUTTON_LEFT)) {
			addCell(placementType, cellPosition(window.getMousePosition()));
			activateNeighbors(cellPosition(window.getMousePosition()));
			change = true;
		}
		if (window.isButtonPressed(Window.BUTTON_RIGHT)) {
			removeCell(cellPosition(window.getMousePosition()));
			activateNeighbors(cellPosition(window.getMousePosition()));
			change = true;
		}
		
		
		
		
		if (window.isKeyPressed(Window.KEY_LEFT)) {
			perspective.turnHorizontal(0.03);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_RIGHT)) {
			perspective.turnHorizontal(-0.03);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_UP)) {
			perspective.turnVertical(0.03);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_DOWN)) {
			perspective.turnVertical(-0.03);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_W)) {
			perspective.moveForward(0.1);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_A)) {
			perspective.moveSideways(-0.1);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_S)) {
			perspective.moveForward(-0.1);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_D)) {
			perspective.moveSideways(0.1);
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_SPACE)) {
			perspective.move(new VectorD(0, 0, 0.1));
			change = true;
		}
		if (window.isKeyPressed(Window.KEY_SHIFT)) {
			perspective.move(new VectorD(0, 0, -0.1));
			change = true;
		}
	}
	
	
	
	
	
	
	
	
	@Override
	public void frame() {
		controller();
		
		if (!change) {
			return;
		}
		
		window.clear();
		
		ArrayList<VectorI> orderedCells = getOrderedList();
		for (VectorI pos : orderedCells) {
			drawCell(pos, getCell(pos));
		}
		
		drawCell(cellPosition(window.getMousePosition()), placementType);
		
		window.update();
		change = false;
	}
	
	private ArrayList<VectorI> getOrderedList() {
		ArrayList<VectorI> cells = new ArrayList<>(allCells.keySet());
		ArrayList<VectorI> orderedCells = new ArrayList<>();
		
		if (cells.size() == 0) {
			return cells;
		}
		
		orderedCells.add(cells.get(0));
		
		for (int i = 1; i < cells.size(); i++) {
			VectorI pos1 = cells.get(i);
			int dist = perspective.getPosition().toVectorI().difference(pos1).getTaxicabMagnitude();
			
			for (int j = 0; j <= orderedCells.size(); j++) {
				if (j == orderedCells.size()) {
					orderedCells.add(pos1);
					break;
				}
				
				int dist2 = perspective.getPosition().toVectorI().difference(orderedCells.get(j)).getTaxicabMagnitude();
				
				if (dist >= dist2) {
					orderedCells.add(j, pos1);
					break;
				}
			}
		}
		
		return orderedCells;
	}
	
	private void drawCell(VectorI cellPosition, Integer type) {
		Color color = ruleSet.getColor(type);
		
		if (activeCells.containsKey(cellPosition)) {
			color = new Color(192, 32, 192, 255);
			
		} else if (cellPosition == null || type == 0) {
			return;
		}
		
		VectorD[] positions = getAdjustedVertices(cellPosition.toVectorD());
		
		Point[] points = new Point[8];
		for (int i = 0; i < 8; i++) {
			points[i] = getScreenPosition(positions[i]);
		}
		
		int[] x = new int[4];
		int[] y = new int[4];
		
		int[] faces = getVisibleFaces(cellPosition);
		
		for (int i = 0; i < faces.length; i++) {
			boolean visible = false;
			
			for (int j = 0; j < 4; j++) {
				x[j] = points[FACE_POINTS[faces[i]][j]].x;
				y[j] = points[FACE_POINTS[faces[i]][j]].y;
				
				visible |= isPointVisible(positions[FACE_POINTS[faces[i]][j]]);
			}
			
			if (visible) {
				window.setColor(shade(color, faces[i]));
				window.fillPolygon(x, y, 4);
			}
		}
		
	}
	
	private VectorD[] getAdjustedVertices(VectorD cellPosition) {
		VectorD[] positions = new VectorD[8];
		
		positions[0] = perspective.getAdjustedPosition(cellPosition.add(0, 0, 0));
		positions[1] = perspective.getAdjustedPosition(cellPosition.add(0, 0, 1));
		positions[2] = perspective.getAdjustedPosition(cellPosition.add(0, 1, 0));
		positions[3] = perspective.getAdjustedPosition(cellPosition.add(0, 1, 1));
		positions[4] = perspective.getAdjustedPosition(cellPosition.add(1, 0, 0));
		positions[5] = perspective.getAdjustedPosition(cellPosition.add(1, 0, 1));
		positions[6] = perspective.getAdjustedPosition(cellPosition.add(1, 1, 0));
		positions[7] = perspective.getAdjustedPosition(cellPosition.add(1, 1, 1));
		
		return positions;
	}
	
	private Point getScreenPosition(VectorD adjustedPos) {
		double factor = SCREEN_DISTANCE * PIXELS_PER_METER / Math.abs(adjustedPos.x);
		return new Point(window.width / 2 + (int) (adjustedPos.y * factor), window.height / 2 - (int) (adjustedPos.z * factor));
	}
	
	private Color shade(Color color, int amount) {
		int shift = SHADING * amount;
		int red = color.getRed() - shift;
		int green = color.getGreen() - shift;
		int blue = color.getBlue() - shift;
		
		return new Color(Math.max(red, 0), Math.max(green, 0), Math.max(blue, 0), color.getAlpha());
	}
	
	private boolean isPointVisible(VectorD adjustedPosition) {
		if (adjustedPosition.x <= SCREEN_DISTANCE) {
			return false;
		}
		
		if (Math.abs(adjustedPosition.x / adjustedPosition.y) <= SCREEN_DISTANCE * PIXELS_PER_METER * 2 / window.width) {
			return false;
		}
		
		if (Math.abs(adjustedPosition.x / adjustedPosition.z) <= SCREEN_DISTANCE * PIXELS_PER_METER * 2 / window.height) {
			return false;
		}
		
		if (adjustedPosition.getMagnitudeSquared() > MAX_VIEW_DISTANCE * MAX_VIEW_DISTANCE) {
			return false;
		}
		
		return true;
	}
	
	private int[] getVisibleFaces(VectorI position) {
		VectorD difference = perspective.getPosition().difference(position);
		boolean[] isFaceVisble = new boolean[6];
		int faceNumber = 0;
		
		if (difference.x > 1 && getCell(position.add(1, 0, 0)) == 0) {
			isFaceVisble[POS_X] = true;
			faceNumber++;
		}
		if (difference.y > 1 && getCell(position.add(0, 1, 0)) == 0) {
			isFaceVisble[POS_Y] = true;
			faceNumber++;
		}
		if (difference.z > 1 && getCell(position.add(0, 0, 1)) == 0) {
			isFaceVisble[POS_Z] = true;
			faceNumber++;
		}
		if (difference.x < 0 && getCell(position.add(-1, 0, 0)) == 0) {
			isFaceVisble[NEG_X] = true;
			faceNumber++;
		}
		if (difference.y < 0 && getCell(position.add(0, -1, 0)) == 0) {
			isFaceVisble[NEG_Y] = true;
			faceNumber++;
		}
		if (difference.z < 0 && getCell(position.add(0, 0, -1)) == 0) {
			isFaceVisble[NEG_Z] = true;
			faceNumber++;
		}
		
		int[] faces = new int[faceNumber];
		faceNumber = 0;
		for (int i = 0; i < 6; i++) {
			if (isFaceVisble[i]) {
				faces[faceNumber] = i;
				faceNumber++;
			}
		}
		
		return faces;
	}
	
	
	
	
	
	
	
	
	private VectorI cellPosition(Point screenPosition) {
		if (screenPosition == null) {
			return null;
		}
		
		double screenX = screenPosition.x - window.width / 2;
		double screenY = window.height / 2 - screenPosition.y;
		double dist = SCREEN_DISTANCE * PIXELS_PER_METER;
		
		double cos = Math.cos(-perspective.getHorizontalAngle());
		double sin = Math.sin(-perspective.getHorizontalAngle());
		
		double theta = perspective.getVerticalAngle() - Math.atan(-screenY / dist);
		
		
		
		
		
		if (theta > 0) {
			double forward = placementDistance / Math.tan(theta);
			double sideways = placementDistance * screenX / (Math.sqrt(dist * dist + screenY * screenY) * Math.sin(theta));
			
			double xDiff = forward * cos - sideways * sin;
			double yDiff = forward * sin + sideways * cos;
			
			return perspective.getPosition().add(xDiff, yDiff, placementDistance).toVectorI();
			
		} else if (theta < 0) {
			double forward = -placementDistance / Math.tan(theta);
			double sideways = -placementDistance * screenX / (Math.sqrt(dist * dist + screenY * screenY) * Math.sin(theta));
			
			double xDiff = forward * cos - sideways * sin;
			double yDiff = forward * sin + sideways * cos;
			
			return perspective.getPosition().add(xDiff, yDiff, -placementDistance).toVectorI();
			
		} else {
			return perspective.getPosition().toVectorI();
			
		}
		
	}
	
	
	
	
	
	
	
	
	@Override
	public void keyTyped(char character) {
	}
	
	@Override
	public void keyPressed(int key) {
	}
	
	@Override
	public void keyReleased(int key) {
	}
	
	@Override
	public void mouseClicked(int button, Point position) {
	}
	
	@Override
	public void mousePressed(int button, Point position) {
	}
	
	@Override
	public void mouseReleased(int button, Point position) {
	}
	
	@Override
	public void mouseMoved(Point oldPosition, Point newPosition) {
		change = true;
	}
	
	@Override
	public void mouseDragged(int button, Point oldPosition, Point newPosition) {
		change = true;
	}
	
	@Override
	public void mouseWheelMoved(int amount) {
		placementDistance += amount;
		
		if (placementDistance <= 1) {
			placementDistance = 2;
		}
	}
	
	@Override
	public void mouseEntered(Point position) {
	}
	
	@Override
	public void mouseExited() {
	}
	
}
