package curtis.bechtel.cellularAutomata;

public class VectorD {
	
	public double x;
	public double y;
	public double z;
	
	public VectorD(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	
	
	
	
	public VectorD add(VectorD vec) {
		return new VectorD(x + vec.x, y + vec.y, z + vec.z);
	}
	
	public VectorD add(VectorI vec) {
		return new VectorD(x + vec.x, y + vec.y, z + vec.z);
	}
	
	public VectorD add(double dx, double dy, double dz) {
		return new VectorD(x + dx, y + dy, z + dz);
	}
	
	
	
	
	
	public VectorD difference(VectorD vec) {
		return new VectorD(x - vec.x, y - vec.y, z - vec.z);
	}
	
	public VectorD difference(VectorI vec) {
		return new VectorD(x - vec.x, y - vec.y, z - vec.z);
	}
	
	public VectorD difference(double dx, double dy, double dz) {
		return new VectorD(x - dx, y - dy, z - dz);
	}
	
	
	
	
	
	public VectorD multiply(double factor) {
		return new VectorD(x * factor, y * factor, z * factor);
	}
	
	
	
	
	
	public double getMagnitudeSquared() {
		return x * x + y * y + z * z;
	}
	
	public double getMagnitude() {
		return Math.sqrt(x * x + y * y + z * z);
	}
	
	public double getTaxicabMagnitude() {
		return Math.abs(x) + Math.abs(y) + Math.abs(z);
	}
	
	
	
	
	
	public VectorI toVectorI() {
		return new VectorI((int) Math.floor(x), (int) Math.floor(y), (int) Math.floor(z));
	}
	
}
