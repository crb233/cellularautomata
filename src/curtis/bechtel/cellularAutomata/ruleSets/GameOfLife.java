package curtis.bechtel.cellularAutomata.ruleSets;

import java.awt.Color;
import curtis.bechtel.cellularAutomata.RuleSet;
import curtis.bechtel.cellularAutomata.Simulator;
import curtis.bechtel.cellularAutomata.VectorI;

public class GameOfLife implements RuleSet {
	
	public static final int DEAD = 0;
	public static final int ALIVE = 1;
	
	@Override
	public Color getColor(int type) {
		if (type == ALIVE) {
			return Color.GREEN;
			
		} else {
			return Color.WHITE;
		}
	}
	
	@Override
	public void updateCell(VectorI position, Simulator simulator) {
		int total = 0;
		
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				for (int z = -1; z <= 1; z++) {
					if (x == 0 && y == 0 && z == 0) {
						continue;
						
					} else if (simulator.getCell(position.add(x, y, z)) == ALIVE) {
						total++;
					}
				}
			}
		}
		
		if (simulator.getCell(position) == ALIVE) {
			if (total != 2 && total != 3) {
				simulator.removeCell(position);
				simulator.addCell(DEAD, position);
				activateNeighbors(position, simulator);
			}
			
		} else if (simulator.getCell(position) == DEAD) {
			if (total == 3) {
				simulator.removeCell(position);
				simulator.addCell(ALIVE, position);
				activateNeighbors(position, simulator);
			}
			
		} else {
			simulator.removeCell(position);
			simulator.addCell(DEAD, position);
		}
	}
	
	private void activateNeighbors(VectorI position, Simulator simulator) {
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				for (int z = -1; z <= 1; z++) {
					if (x == 0 && y == 0 && z == 0) {
						continue;
						
					} else {
						simulator.activateCell(position.add(x, y, z));
					}
				}
			}
		}
		
	}
	
}
