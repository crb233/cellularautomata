package curtis.bechtel.cellularAutomata.ruleSets;

import java.awt.Color;
import curtis.bechtel.cellularAutomata.RuleSet;
import curtis.bechtel.cellularAutomata.Simulator;
import curtis.bechtel.cellularAutomata.VectorI;

public class Lasers implements RuleSet {
	
	public static final int EMPTY = 0; // empty cell
	public static final int WALL = 1; // wall : blocks lasers
	public static final int SOURCE = 2; // laser source
	public static final int POS_X = 3; // laser directed toward positive x
	public static final int POS_Y = 4; // laser directed toward positive y
	public static final int POS_Z = 5; // laser directed toward positive z
	public static final int NEG_X = 6; // laser directed toward negative x
	public static final int NEG_Y = 7; // laser directed toward negative y
	public static final int NEG_Z = 8; // laser directed toward negative z
	public static final int OFF = 9; // off power source
	public static final int ON = 10; // on power source
	
	@Override
	public Color getColor(int type) {
		switch (type) {
			case WALL:
				return Color.LIGHT_GRAY;
				
			case SOURCE:
				return Color.RED;
				
			case POS_X:
			case POS_Y:
			case POS_Z:
			case NEG_X:
			case NEG_Y:
			case NEG_Z:
				return Color.YELLOW;
				
			default:
				return Color.WHITE;
		}
	}
	
	@Override
	public void updateCell(VectorI position, Simulator simulator) {
		boolean posX = false;
		boolean posY = false;
		boolean posZ = false;
		boolean negX = false;
		boolean negY = false;
		boolean negZ = false;
		
		switch (simulator.getCell(position.add(-1, 0, 0))) {
			case SOURCE:
			case POS_X:
				posX = true;
		}
		switch (simulator.getCell(position.add(0, -1, 0))) {
			case SOURCE:
			case POS_Y:
				posY = true;
		}
		switch (simulator.getCell(position.add(0, 0, -1))) {
			case SOURCE:
			case POS_Z:
				posZ = true;
		}
		switch (simulator.getCell(position.add(1, 0, 0))) {
			case SOURCE:
			case NEG_X:
				negX = true;
		}
		switch (simulator.getCell(position.add(0, 1, 0))) {
			case SOURCE:
			case NEG_Y:
				negY = true;
		}
		switch (simulator.getCell(position.add(0, 0, 1))) {
			case SOURCE:
			case NEG_Z:
				negZ = true;
		}
		
		
		
		
		
		switch (simulator.getCell(position)) {
			case EMPTY:
				if (posY == negY && posZ == negZ) {
					if (posX && !negX) {
						simulator.addCell(POS_X, position);
						simulator.activateCell(position.add(1, 0, 0));
						
					} else if (negX && !posX) {
						simulator.addCell(NEG_X, position);
						simulator.activateCell(position.add(-1, 0, 0));
						
					} else {
						simulator.addCell(EMPTY, position);
						
					}
				} else if (posX == negX && posZ == negZ) {
					if (posY && !negY) {
						simulator.addCell(POS_Y, position);
						simulator.activateCell(position.add(0, 1, 0));
						
					} else if (negY && !posY) {
						simulator.addCell(NEG_Y, position);
						simulator.activateCell(position.add(0, -1, 0));
						
					} else {
						simulator.addCell(EMPTY, position);
						
					}
				} else if (posX == negX && posY == negY) {
					if (posZ && !negZ) {
						simulator.addCell(POS_Z, position);
						simulator.activateCell(position.add(0, 0, 1));
						
					} else if (negZ && !posZ) {
						simulator.addCell(NEG_Z, position);
						simulator.activateCell(position.add(0, 0, -1));
						
					} else {
						simulator.addCell(EMPTY, position);
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					
				}
				break;
			
			case POS_X:
				if (posY == negY && posZ == negZ) {
					if (posX && !negX) {
						simulator.addCell(POS_X, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(1, 0, 0));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(1, 0, 0));
					
				}
				break;
			
			case POS_Y:
				if (posX == negX && posZ == negZ) {
					if (posY && !negY) {
						simulator.addCell(POS_Y, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(0, 1, 0));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(0, 1, 0));
					
				}
				break;
			
			case POS_Z:
				if (posX == negX && posY == negY) {
					if (posZ && !negZ) {
						simulator.addCell(POS_Z, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(0, 0, 1));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(0, 0, 1));
					
				}
				break;
			
			case NEG_X:
				if (posY == negY && posZ == negZ) {
					if (negX && !posX) {
						simulator.addCell(NEG_X, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(-1, 0, 0));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(-1, 0, 0));
					
				}
				break;
			
			case NEG_Y:
				if (posX == negX && posZ == negZ) {
					if (negY && !posY) {
						simulator.addCell(NEG_Y, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(0, -1, 0));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(0, -1, 0));
					
				}
				break;
			
			case NEG_Z:
				if (posX == negX && posY == negY) {
					if (negZ && !posZ) {
						simulator.addCell(NEG_Z, position);
						
					} else {
						simulator.addCell(EMPTY, position);
						simulator.activateCell(position.add(0, 0, -1));
						
					}
				} else {
					simulator.addCell(EMPTY, position);
					simulator.activateCell(position.add(0, 0, -1));
					
				}
				break;
			
			case SOURCE:
				if (simulator.getCell(position.add(1, 0, 0)) == EMPTY) {
					simulator.activateCell(position.add(1, 0, 0));
				}
				if (simulator.getCell(position.add(0, 1, 0)) == EMPTY) {
					simulator.activateCell(position.add(0, 1, 0));
				}
				if (simulator.getCell(position.add(0, 0, 1)) == EMPTY) {
					simulator.activateCell(position.add(0, 0, 1));
				}
				if (simulator.getCell(position.add(-1, 0, 0)) == EMPTY) {
					simulator.activateCell(position.add(-1, 0, 0));
				}
				if (simulator.getCell(position.add(0, -1, 0)) == EMPTY) {
					simulator.activateCell(position.add(0, -1, 0));
				}
				if (simulator.getCell(position.add(0, 0, -1)) == EMPTY) {
					simulator.activateCell(position.add(0, 0, -1));
				}
				break;
		}
		
		simulator.deactivateCell(position);
	}
	
}
