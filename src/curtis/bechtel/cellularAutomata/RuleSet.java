package curtis.bechtel.cellularAutomata;

import java.awt.Color;

public interface RuleSet {
	
	public abstract Color getColor(int type);
	
	public abstract void updateCell(VectorI position, Simulator simulator);
	
}
